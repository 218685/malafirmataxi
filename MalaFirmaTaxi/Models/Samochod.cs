﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MalaFirmaTaxi.Models
{
    public class Samochod
    {
	    public int ID { get; set; }

	    [Required(ErrorMessage = "To pole jest wymagane")]
	    [StringLength(30, MinimumLength = 2, ErrorMessage = "Marka nie może być dłuższa niż 30 znaków")]
	    [Display(Name = "Marka")]
	    public string Marka { get; set; }

	    [Required(ErrorMessage = "To pole jest wymagane")]
	    [StringLength(50, MinimumLength = 1, ErrorMessage = "Model nie może być dłuższy niż 30 znaków")]
	    [Display(Name = "Model")]
	    public string Model { get; set; }

	    [Required(ErrorMessage = "To pole jest wymagane")]
	    [Display(Name = "Liczba miejsc")]
		[Range(1,8, ErrorMessage = "Liczba miejsc musi wynosić od 1 do 8")]
		public int LiczbaMiejsc { get; set; }

	    [Required(ErrorMessage = "To pole jest wymagane")]
	    [Display(Name = "Koszt za 1 km")]
	    [DataType(DataType.Currency)]
	    [Column(TypeName = "money")]
		public decimal Koszt1Km { get; set; }

	    public string Nazwa => Marka + " " + Model;

	    public Kierowca Kierowca { get; set; }
	}

}
