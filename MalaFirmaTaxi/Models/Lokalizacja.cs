﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MalaFirmaTaxi.Models
{
	public class Lokalizacja
	{
		public int ID { get; set; }

		[Required(ErrorMessage = "To pole jest wymagane")]
		[StringLength(50, MinimumLength = 3, ErrorMessage = "Ulica musi mieć od 3 do 50 znaków")]
		public string Ulica { get; set; }

		[Required(ErrorMessage = "To pole jest wymagane")]
		[Range(1,int.MaxValue, ErrorMessage = "Numer ulicy nie może być ujemny")]
		public int Numer { get; set; }

		[Required(ErrorMessage = "To pole jest wymagane")]
		[StringLength(40, MinimumLength = 3, ErrorMessage = "Ulica musi mieć od 3 do 40 znaków")]
		public string Miasto { get; set; }

		public string DlugoscGeogr { get; set; }
		public string SzerokoscGeogr { get; set; }

		[Display(Name = "Adres")]
		public string Adres => "ul. " + Ulica + " " + Numer + ", " + Miasto;

		public ICollection<Transakcja> Transakcje { get; set; }
		public ICollection<Zlecenie> ZleceniaStart { get; set; }
		public ICollection<Zlecenie> ZleceniaCel { get; set; }
	}
}
