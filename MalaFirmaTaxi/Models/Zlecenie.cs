﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.ApplicationInsights.AspNetCore;

namespace MalaFirmaTaxi.Models
{
    public class Zlecenie
    {
	    public int ID { get; set; }

	    [Required(ErrorMessage = "To pole jest wymagane")]
		public int KlientID { get; set; }

		public int? KierowcaID { get; set; }

	    [Display(Name = "Koszt kursu")]
	    [DataType(DataType.Currency)]
	    [Column(TypeName = "money")]
		public decimal Koszt { get; set; }

	    [Required(ErrorMessage = "To pole jest wymagane")]
		public int LokalizacjaStartID { get; set; }

	    [Required(ErrorMessage = "To pole jest wymagane")]
		public int LokalizacjaCelID { get; set; }

	    [Range(1, 4, ErrorMessage = "Liczba osób musi wynosić od 1 do 4")]
		public int LiczbaOsob { get; set; }

	    public ICollection<Transakcja> Transakcje { get; set; }
	    public Kierowca Kierowca { get; set; }
	    public Klient Klient { get; set; }
		public Lokalizacja LokalizacjaStart { get; set; }
		public Lokalizacja LokalizacjaCel { get; set; }

		public Zlecenie()
	    {
		    LiczbaOsob = 1;
	    }
	}
}
