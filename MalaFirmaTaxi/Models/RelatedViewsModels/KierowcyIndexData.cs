﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MalaFirmaTaxi.Models.RelatedViewsModels
{
    public class KierowcyIndexData
    {
	    public IEnumerable<Kierowca> Kierowcy { get; set; }
		public IEnumerable<Zlecenie> Zlecenia { get; set; }
		public IEnumerable<Transakcja> Transakcje { get; set; }
	}
}
