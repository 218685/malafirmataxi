﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MalaFirmaTaxi.Models.RelatedViewsModels
{
    public class KlienciIndexData
    {
	    public IEnumerable<Klient> Klienci { get; set; }
		public IEnumerable<Zlecenie> Zlecenia { get; set; }
		public IEnumerable<Transakcja> Transakcje { get; set; }
	}
}
