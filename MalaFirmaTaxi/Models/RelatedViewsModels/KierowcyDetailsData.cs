﻿using System.Collections.Generic;

namespace MalaFirmaTaxi.Models.RelatedViewsModels
{
    public class KierowcyDetailsData
    {
	    public Kierowca Kierowca { get; set; }
		public IEnumerable<Zlecenie> Zlecenia { get; set; }
		public IEnumerable<Transakcja> Transakcje { get; set; }
	    public IEnumerable<Transakcja> TransakcjeBezZlecenia { get; set; }
		public Zlecenie NoweZlecenie { get; set; }
	    public Zlecenie AwaryjneZlecenie { get; set; }
		public bool JestAwariaNowegoZlecenia { get; set; }
	}
}
