﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MalaFirmaTaxi.Models
{
    public class Klient
    {
	    public int ID { get; set; }

		[StringLength(30, MinimumLength = 2, ErrorMessage = "Imię musi mieć od 2 do 30 znaków.")]
	    [Display(Name = "Imię")]
	    public string Imie { get; set; }

		[StringLength(40, MinimumLength = 2, ErrorMessage = "Nazwisko musi mieć od 2 do 30 znaków.")]
	    [Display(Name = "Nazwisko")]
	    public string Nazwisko { get; set; }

	    [Required(ErrorMessage = "To pole jest wymagane")]
		[StringLength(9, MinimumLength = 9, ErrorMessage = "Numer telefonu musi mieć dokładnie 9 cyfr.")]
		public string Telefon { get; set; }
		
	    public string FullName => Imie + " " + Nazwisko;

		public ICollection<Zlecenie> Zlecenia { get; set; }
	}
}
