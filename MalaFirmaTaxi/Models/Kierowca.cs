﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MalaFirmaTaxi.Models
{
    public class Kierowca
    {
	    public int ID { get; set; }

	    [Required(ErrorMessage = "To pole jest wymagane")]
		[StringLength(30, MinimumLength = 2, ErrorMessage = "Imię musi mieć od 2 do 30 znaków.")]
	    [Display(Name = "Imię")]
	    public string Imie { get; set; }

	    [Required(ErrorMessage = "To pole jest wymagane")]
		[StringLength(40, MinimumLength = 3, ErrorMessage = "Nazwisko musi mieć od 3 do 30 znaków.")]
	    [Display(Name = "Nazwisko")]
	    public string Nazwisko { get; set; }

	    [Required(ErrorMessage = "To pole jest wymagane")]
		public int SamochodID { get; set; }

	    public string FullName => Imie + " " + Nazwisko;

	    public Samochod Samochod { get; set; }

	    public ICollection<Zlecenie> Zlecenia { get; set; }
	    
		public ICollection<Transakcja> Transakcje { get; set; }
	}
}
