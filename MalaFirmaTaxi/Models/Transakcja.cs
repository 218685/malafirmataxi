﻿using System;
using System.ComponentModel.DataAnnotations;

namespace MalaFirmaTaxi.Models
{
	public enum Rodzaj
	{
		[Display(Name = "Aktywny")]
		Aktywny,
		[Display(Name = "Nieaktywny")]
		Nieaktywny,
		[Display(Name = "Przyjęcie zlecenia")] 
		PrzyjecieZlecenia,
		[Display(Name = "Odbiór pasażera")] 
		OdbiorPasazera,
		[Display(Name = "Koniec kursu")] 
		KoniecKursu,
		[Display(Name = "Awaria")] 
		Awaria,
		[Display(Name = "Awaryjne przyjęcie zlecenia")]
		AwaryjnePrzyjecieZlecenia,
		[Display(Name = "Przejęcie pasażera po awarii")]
		PrzejeciePasazeraPoAwarii,
	}

	public class Transakcja
    {
	    public int ID { get; set; }
	    [Required(ErrorMessage = "To pole jest wymagane")]
		public int KierowcaID { get; set; }
		[Required(ErrorMessage = "To pole jest wymagane")]
		public int LokalizacjaID { get; set; }
	 
		public int? ZlecenieID { get; set; }

	    [DataType(DataType.DateTime)]
	    [Display(Name = "Czas")]
		public DateTime StempelCzasu { get; set; }
	    
		[Required(ErrorMessage = "To pole jest wymagane")]
		public Rodzaj Rodzaj { get; set; }

	    public string Uwagi { get; set; }

		public Kierowca Kierowca { get; set; }
		public Zlecenie Zlecenie { get; set; }
		public Lokalizacja Lokalizacja { get; set; }

	    public Transakcja()
	    {
		    StempelCzasu = DateTime.Now;
	    }
	}
}
