﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using MalaFirmaTaxi.Models;
using Microsoft.EntityFrameworkCore.Metadata;

namespace MalaFirmaTaxi.Data
{
	public class TaxiContext : DbContext
	{
		public TaxiContext(DbContextOptions<TaxiContext> options) : base(options)
		{
		}

		public DbSet<Klient> Klienci { get; set; }
		public DbSet<Kierowca> Kierowcy { get; set; }
		public DbSet<Samochod> Samochody { get; set; }
		public DbSet<Zlecenie> Zlecenia { get; set; }
		public DbSet<Transakcja> Transakcje { get; set; }
		public DbSet<Lokalizacja> Lokalizacje { get; set; }

		protected override void OnModelCreating(ModelBuilder modelBuilder)
		{
			modelBuilder.Entity<Zlecenie>()
				.HasOne(p => p.LokalizacjaStart)
				.WithMany(b => b.ZleceniaStart)
				.OnDelete(DeleteBehavior.Restrict);

			modelBuilder.Entity<Zlecenie>()
				.HasOne(p => p.LokalizacjaCel)
				.WithMany(b => b.ZleceniaCel)
				.OnDelete(DeleteBehavior.Restrict);

			modelBuilder.Entity<Transakcja>()
				.HasOne(p => p.Lokalizacja)
				.WithMany(b => b.Transakcje)
				.OnDelete(DeleteBehavior.Restrict);

			modelBuilder.Entity<Klient>().ToTable("Klienci");
			modelBuilder.Entity<Kierowca>().ToTable("Kierowcy");
			modelBuilder.Entity<Samochod>().ToTable("Samochody");
			modelBuilder.Entity<Zlecenie>().ToTable("Zlecenia");
			modelBuilder.Entity<Transakcja>().ToTable("Transakcje");
			modelBuilder.Entity<Lokalizacja>().ToTable("Lokalizacje");
		}

		public Zlecenie FindZlecenieById(int id)
		{
			return Zlecenia.Find(id);
		}

		public Lokalizacja FindLokalizacjaById(int id)
		{
			return Lokalizacje.Find(id);
		}

		public Klient FindKlientById(int id)
		{
			return Klienci.Find(id);
		}

		public Klient FindKlientByTelefon(string numer)
		{
			return Klienci.SingleOrDefault(s => s.Telefon == numer);
		}

		public Kierowca FindKierowcaById(int id)
		{
			return Kierowcy.Find(id);
		}

		public Samochod FindSamochodById(int id)
		{
			return Samochody.Find(id);
		}

		public int FindRecentLokalizacjaOfKierowca(int kierowcaId)
		{
			var transakcje = Transakcje.Where(s => s.KierowcaID == kierowcaId).OrderByDescending(i => i.StempelCzasu);
			var ostatniaLokalizacjaId = transakcje.First().LokalizacjaID;
			return ostatniaLokalizacjaId;
		}

		public decimal FindKosztZa1KmOfSamochodOfKierowca(int kierowcaId)
		{
			var kierowca = FindKierowcaById(kierowcaId);
			var samochod = FindSamochodById(kierowca.SamochodID);
			return samochod.Koszt1Km;
		}

		public int? FindNearestAvailableKierowcaForZlecenie(int zlecenieId)
		{
			var zlecenie = FindZlecenieById(zlecenieId);
			var startId = zlecenie.LokalizacjaStartID;

			var dostepniKierowcy = GetListOfAvailableKierowcy();

			double minOdleglosc = 999999;
			var najblizszyKierowcaId = 1;

			foreach (var id in dostepniKierowcy)
			{
				var lokalizacjaKierowcyId = FindRecentLokalizacjaOfKierowca(id);
				var odlegloscOdStartu = CalculateDistanceBetween(startId, lokalizacjaKierowcyId);
				if (!(odlegloscOdStartu < minOdleglosc))
					continue;
				minOdleglosc = odlegloscOdStartu;
				najblizszyKierowcaId = id;
			}

			return najblizszyKierowcaId;
		}

		public IEnumerable<int> GetListOfAvailableKierowcy()
		{
			var dostepniKierowcyId = new List<int>();

			foreach (var kierowca in Kierowcy)
			{
				var transakcjeKierowcy = Transakcje
					.Where(s => s.KierowcaID == kierowca.ID)
					.OrderByDescending(i => i.StempelCzasu);

				if (!transakcjeKierowcy.Any())
					continue;

				var ostatniaTransakcja = transakcjeKierowcy.First();

				if (ostatniaTransakcja.Rodzaj == Rodzaj.Aktywny || ostatniaTransakcja.Rodzaj == Rodzaj.KoniecKursu)
				{
					dostepniKierowcyId.Add(kierowca.ID);
				}
			}
			
			return dostepniKierowcyId;
		}
		
		public double CalculateDistanceBetween(int lokalizacjaId1, int lokalizacjaId2)
		{
			var lok1 = FindLokalizacjaById(lokalizacjaId1);
			var lok2 = FindLokalizacjaById(lokalizacjaId2);
			return CalculateDistanceBetween(lok1, lok2);
		}
		
		public double CalculateDistanceBetween(Lokalizacja lokalizacja1, Lokalizacja lokalizacja2)
		{
			return new Random().NextDouble() * 30 + 1;
		}

		public decimal CalculateCostOfZlecenie(Zlecenie zlecenie)
		{
			var start = zlecenie.LokalizacjaStartID;
			var cel = zlecenie.LokalizacjaCelID;
			var odleglosc = CalculateDistanceBetween(start, cel);
			var kierowcaId = zlecenie.KierowcaID.Value;
			var stawkaZa1Km = FindKosztZa1KmOfSamochodOfKierowca(kierowcaId);

			var kosztZlecenia = (int)odleglosc * stawkaZa1Km;

			return kosztZlecenia;
		}



		public void AddPrzyjecieZleceniaForZlecenie(int zlecenieId)
		{
			var zlecenie = FindZlecenieById(zlecenieId);
			var kierowcaId = zlecenie.KierowcaID.Value;
			var transakcja = new Transakcja
			{
				ZlecenieID = zlecenieId,
				KierowcaID = kierowcaId,
				LokalizacjaID = FindRecentLokalizacjaOfKierowca(kierowcaId),
				Rodzaj = Rodzaj.PrzyjecieZlecenia,
				Uwagi = "Automatyczne dopasowanie"
			};

			Add(transakcja);
			SaveChanges();
		}

		public void AddAwaryjnePrzyjecieZleceniaForZlecenie(int zlecenieId)
		{
			var kierowcaId = FindNearestAvailableKierowcaForZlecenie(zlecenieId);
			var transakcja = new Transakcja
			{
				ZlecenieID = zlecenieId,
				KierowcaID = kierowcaId.Value,
				LokalizacjaID = FindRecentLokalizacjaOfKierowca(kierowcaId.Value),
				Rodzaj = Rodzaj.AwaryjnePrzyjecieZlecenia,
				Uwagi = "Automatyczne dopasowanie"
			};

			Add(transakcja);
			SaveChanges();
		}

		public int? CheckForRecentZlecenieForKierowca(int kierowcaId)
		{
			var ostatnieTransakcje = Transakcje
				.Where(s => s.KierowcaID == kierowcaId && s.ZlecenieID != null && s.Rodzaj == Rodzaj.PrzyjecieZlecenia)
				.OrderByDescending(i => i.StempelCzasu);

			foreach (var transakcja in ostatnieTransakcje)
			{
				if (ZlecenieJestSkonczone(transakcja.ZlecenieID.Value))
					continue;

				return transakcja.ZlecenieID;
			}

			return null;
		}

		public bool ZlecenieJestSkonczone(int zlecenieId)
		{
			var ostatnieTransakcje = Transakcje
				.Where(s => s.ZlecenieID == zlecenieId && s.Rodzaj == Rodzaj.KoniecKursu);

			return ostatnieTransakcje.Any();
		}

		public int? CheckForAwaryjneZlecenieForKierowca(int kierowcaId)
		{
			var awaryjneTransakcje = Transakcje
				.Where(s => s.KierowcaID == kierowcaId && s.ZlecenieID != null && s.Rodzaj == Rodzaj.AwaryjnePrzyjecieZlecenia)
				.OrderByDescending(i => i.StempelCzasu);

			foreach (var transakcja in awaryjneTransakcje)
			{
				if (ZlecenieJestSkonczone(transakcja.ZlecenieID.Value))
					continue;

				return transakcja.ZlecenieID;
			}

			return null;
		}

		public int FindAwaryjnaLokalizacjaIdOfZlecenie(int? awaryjneZlecenieId)
		{
			var awaryjnaTransakcja = Transakcje
				.First(s => s.ZlecenieID == awaryjneZlecenieId.Value && s.Rodzaj == Rodzaj.Awaria);

			return awaryjnaTransakcja.LokalizacjaID;
		}

		public Lokalizacja FindAwaryjnaLokalizacjaOfZlecenie(int? awaryjneZlecenieId)
		{
			var awaryjnaTransakcja = Transakcje
				.First(s => s.ZlecenieID == awaryjneZlecenieId.Value && s.Rodzaj == Rodzaj.Awaria);

			var awaryjnaLokalizacjaId = awaryjnaTransakcja.LokalizacjaID;
			return FindLokalizacjaById(awaryjnaLokalizacjaId);
		}

		public bool TransakcjaAlreadyExists(Transakcja transakcja)
		{
			return Transakcje.Any(e => e.KierowcaID == transakcja.KierowcaID
									   && e.Rodzaj == transakcja.Rodzaj
									   && e.ZlecenieID == transakcja.ZlecenieID
									   && e.LokalizacjaID == transakcja.LokalizacjaID);
		}

		public bool SamochodIsFree(int samochodId)
		{
			return Kierowcy.Any(e => e.SamochodID == samochodId);
		}

		public bool AwaryjnaTransakcjaWZleceniu(int zlecenieId)
		{
			return Transakcje.Any(e => e.ZlecenieID == zlecenieId
									   && e.Rodzaj == Rodzaj.Awaria);
		}

		public bool AwaryjnaTransakcjaKierowcyWZleceniu(int zlecenieId, int kierowcaId)
		{
			return Transakcje.Any(e => e.ZlecenieID == zlecenieId
									   && e.KierowcaID == kierowcaId
									   && e.Rodzaj == Rodzaj.Awaria);
		}

		public bool OdebranoPasazeraWZleceniu(int zlecenieId)
		{
			return Transakcje.Any(e => e.ZlecenieID == zlecenieId
									   && e.Rodzaj == Rodzaj.OdbiorPasazera);
		}

		public bool AwariaLubNieaktywnyKierowca(int kierowcaId)
		{
			var ostatniaTransakcja = Transakcje
				.Where(s => s.KierowcaID == kierowcaId)
				.OrderByDescending(i => i.StempelCzasu)
				.FirstOrDefault();

			return ostatniaTransakcja == null || ostatniaTransakcja.Rodzaj == Rodzaj.Awaria || ostatniaTransakcja.Rodzaj == Rodzaj.Nieaktywny;
		}

		public bool OdebranoAwaryjniePasazeraWZleceniu(int zlecenieId)
		{
			return Transakcje.Any(e => e.ZlecenieID == zlecenieId
			                           && e.Rodzaj == Rodzaj.PrzejeciePasazeraPoAwarii);
		}

		public int? GetNoweZlecenieKlienta(int id)
		{
			var zleceniaKlienta = Zlecenia.Where(z => z.KlientID == id);

			foreach (var zlecenie in zleceniaKlienta)
			{
				if (ZlecenieJestSkonczone(zlecenie.ID))
					continue;

				var ostatniaTransakjaZlecenia = Transakcje
					.Where(t => t.ZlecenieID == zlecenie.ID)
					.OrderByDescending(t => t.StempelCzasu)
					.FirstOrDefault();

				if (ostatniaTransakjaZlecenia.Rodzaj == Rodzaj.PrzyjecieZlecenia)
					return ostatniaTransakjaZlecenia.ZlecenieID;
			}

			return null;
		}

		public int CalculateEstimatedArrivalTime(Zlecenie zlecenie)
		{
			var kierowcaId = zlecenie.KierowcaID.Value;
			var kosztZa1Km = FindKosztZa1KmOfSamochodOfKierowca(kierowcaId);

			var calkowityKoszt = zlecenie.Koszt;

			var odlegloscLokalizacji = calkowityKoszt/kosztZa1Km;

			const double predkoscNaMinute = 5.0 / 6.0;

			return (int) (Convert.ToDouble(odlegloscLokalizacji) / predkoscNaMinute);
		}
	}
}
