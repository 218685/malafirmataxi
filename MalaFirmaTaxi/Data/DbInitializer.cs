﻿using MalaFirmaTaxi.Models;
using System;
using System.Linq;

namespace MalaFirmaTaxi.Data
{
	public static class DbInitializer
	{
		public static void Initialize(TaxiContext context)
		{
			context.Database.EnsureCreated();

			// Look for any students.
			if (context.Klienci.Any())
			{
				return;   // DB has been seeded
			}

			var klienci = new Klient[]
			{
				new Klient{Imie="Grzegorz",Nazwisko="Schetyna",Telefon="668765432"},
				new Klient{Imie="Ryszard",Nazwisko="Pietru",Telefon="234657848"},
				new Klient{Imie="Ewa",Nazwisko="Kopacz",Telefon="928363222"},
				new Klient{Imie="Paweł",Nazwisko="Kukiz",Telefon="555747333"},
				new Klient{Imie="Donald",Nazwisko="Tusk",Telefon="666666666"},
			};
			foreach (Klient k in klienci)
			{
				context.Klienci.Add(k);
			}
			context.SaveChanges();

			var samochody = new Samochod[]
			{
				new Samochod{Marka = "Opel",Model="Astra",LiczbaMiejsc = 4, Koszt1Km = 0.45M},
				new Samochod{Marka = "Mercedes-Benz", Model="Klasa E",LiczbaMiejsc = 3, Koszt1Km = 0.70M},
				new Samochod{Marka = "Dacia", Model="Logan",LiczbaMiejsc = 4, Koszt1Km = 0.22M},
				new Samochod{Marka = "Volkswagen",Model="Passat",LiczbaMiejsc = 4, Koszt1Km = 0.55M},
				new Samochod{Marka = "Ford",Model="Transit",LiczbaMiejsc = 6, Koszt1Km = 0.60M},
			};
			foreach (Samochod s in samochody)
			{
				context.Samochody.Add(s);
			}
			context.SaveChanges();

			var kierowcy = new Kierowca[]
			{
				new Kierowca{Imie="Jarosław",Nazwisko="Kaczyński",SamochodID = samochody.Single( s => s.Marka == "Mercedes-Benz").ID},
				new Kierowca{Imie="Andrzej",Nazwisko="Duda",SamochodID = samochody.Single( s => s.Marka == "Opel").ID},
				new Kierowca{Imie="Antoni",Nazwisko="Macierewicz",SamochodID = samochody.Single( s => s.Marka == "Ford").ID},
				new Kierowca{Imie="Beata",Nazwisko="Szydło",SamochodID = samochody.Single( s => s.Marka == "Volkswagen").ID},
			};
			foreach (Kierowca k in kierowcy)
			{
				context.Kierowcy.Add(k);
			}
			context.SaveChanges();

			var lokalizacje = new Lokalizacja[]
			{
				new Lokalizacja{Ulica = "Nowowiejska", Numer = 48, Miasto = "Wrocław"},
				new Lokalizacja{Ulica = "Prądnicka", Numer = 28, Miasto = "Wrocław"},
				new Lokalizacja{Ulica = "Słowiańska", Numer = 2, Miasto = "Wrocław"},
				new Lokalizacja{Ulica = "Reja", Numer = 1, Miasto = "Wrocław"},
				new Lokalizacja{Ulica = "Mućka", Numer = 44, Miasto = "Wrocław"},
				new Lokalizacja{Ulica = "Reja", Numer = 13, Miasto = "Wrocław"},
				new Lokalizacja{Ulica = "Powstańców", Numer = 2, Miasto = "Wrocław"},
				new Lokalizacja{Ulica = "Piłsudskiego", Numer = 22, Miasto = "Wrocław"},
				new Lokalizacja{Ulica = "Lizaka", Numer = 12, Miasto = "Wrocław"},
				new Lokalizacja{Ulica = "Zawalna", Numer = 32, Miasto = "Wrocław"},
			};
			foreach (Lokalizacja lok in lokalizacje)
			{
				context.Lokalizacje.Add(lok);
			}
			context.SaveChanges();

			var zlecenia = new Zlecenie[]
			{
				new Zlecenie{KierowcaID = 1, KlientID = 4, LokalizacjaStartID = 10, LokalizacjaCelID = 1},
				new Zlecenie{KierowcaID = 2, KlientID = 3, LokalizacjaStartID = 6, LokalizacjaCelID = 3},
				new Zlecenie{KierowcaID = 3, KlientID = 2, LokalizacjaStartID = 6, LokalizacjaCelID = 5},
				new Zlecenie{KierowcaID = 4, KlientID = 1, LokalizacjaStartID = 4, LokalizacjaCelID = 7},
				new Zlecenie{KierowcaID = 1, KlientID = 5, LokalizacjaStartID = 2, LokalizacjaCelID = 9},
			};
			foreach (Zlecenie z in zlecenia)
			{
				context.Zlecenia.Add(z);
			}
			context.SaveChanges();

			var transakcje = new Transakcja[]
			{
				new Transakcja
				{
					KierowcaID = 1, ZlecenieID = 1, LokalizacjaID = 6,
					StempelCzasu = DateTime.Now.AddMinutes(2), Rodzaj = Rodzaj.PrzyjecieZlecenia
				},
				new Transakcja
				{
					KierowcaID = 1, ZlecenieID = 1, LokalizacjaID = zlecenia.Single(c=>c.ID==1).LokalizacjaStartID,
					StempelCzasu = DateTime.Now.AddMinutes(10), Rodzaj = Rodzaj.OdbiorPasazera
				},
				new Transakcja
				{
					KierowcaID = 1, ZlecenieID = 1, LokalizacjaID = zlecenia.Single(c=>c.ID==1).LokalizacjaCelID,
					StempelCzasu = DateTime.Now.AddMinutes(45), Rodzaj = Rodzaj.KoniecKursu
				},

				new Transakcja
				{
					KierowcaID = 2, ZlecenieID = 2, LokalizacjaID = 10,
					StempelCzasu = DateTime.Now, Rodzaj = Rodzaj.PrzyjecieZlecenia
				},
				new Transakcja
				{
					KierowcaID = 2, ZlecenieID = 2, LokalizacjaID = zlecenia.Single(c=>c.ID==3).LokalizacjaStartID,
					StempelCzasu = DateTime.Now.AddMinutes(8), Rodzaj = Rodzaj.OdbiorPasazera
				},
				new Transakcja
				{
					KierowcaID = 2, ZlecenieID = 2, LokalizacjaID = zlecenia.Single(c=>c.ID==3).LokalizacjaCelID,
					StempelCzasu = DateTime.Now.AddMinutes(33), Rodzaj = Rodzaj.KoniecKursu
				},

				new Transakcja
				{
					KierowcaID = 3, ZlecenieID = 3, LokalizacjaID = 7,
					StempelCzasu = DateTime.Now.AddMinutes(9), Rodzaj = Rodzaj.PrzyjecieZlecenia
				},
				new Transakcja
				{
					KierowcaID = 3, ZlecenieID = 3, LokalizacjaID = zlecenia.Single(c=>c.ID==3).LokalizacjaStartID,
					StempelCzasu = DateTime.Now.AddMinutes(15), Rodzaj = Rodzaj.OdbiorPasazera
				},
				new Transakcja
				{
					KierowcaID = 3, ZlecenieID = 3, LokalizacjaID = zlecenia.Single(c=>c.ID==3).LokalizacjaCelID,
					StempelCzasu = DateTime.Now.AddMinutes(28), Rodzaj = Rodzaj.KoniecKursu
				},

				new Transakcja
				{
					KierowcaID = 4, ZlecenieID = 4, LokalizacjaID = 4,
					StempelCzasu = DateTime.Now.AddMinutes(3), Rodzaj = Rodzaj.PrzyjecieZlecenia
				},
				new Transakcja
				{
					KierowcaID = 4, ZlecenieID = 4, LokalizacjaID = zlecenia.Single(c=>c.ID==3).LokalizacjaStartID,
					StempelCzasu = DateTime.Now.AddMinutes(25), Rodzaj = Rodzaj.OdbiorPasazera
				},
				new Transakcja
				{
					KierowcaID = 4, ZlecenieID = 4, LokalizacjaID = zlecenia.Single(c=>c.ID==3).LokalizacjaCelID,
					StempelCzasu = DateTime.Now.AddMinutes(55), Rodzaj = Rodzaj.KoniecKursu
				},

				new Transakcja
				{
					KierowcaID = 1, ZlecenieID = 5, LokalizacjaID = 9,
					StempelCzasu = DateTime.Now.AddMinutes(10), Rodzaj = Rodzaj.PrzyjecieZlecenia
				},
				new Transakcja
				{
					KierowcaID = 1, ZlecenieID = 5, LokalizacjaID = zlecenia.Single(c=>c.ID==5).LokalizacjaStartID,
					StempelCzasu = DateTime.Now.AddMinutes(16), Rodzaj = Rodzaj.OdbiorPasazera
				},
				new Transakcja
				{
					KierowcaID = 1, ZlecenieID = 5, LokalizacjaID = 5,
					StempelCzasu = DateTime.Now.AddMinutes(45), Rodzaj = Rodzaj.Awaria
				},
				new Transakcja
				{
					KierowcaID = 2, ZlecenieID = 5, LokalizacjaID = 5,
					StempelCzasu = DateTime.Now.AddMinutes(55), Rodzaj = Rodzaj.PrzejeciePasazeraPoAwarii
				},
				new Transakcja
				{
					KierowcaID = 2, ZlecenieID = 5, LokalizacjaID = zlecenia.Single(c=>c.ID==5).LokalizacjaCelID,
					StempelCzasu = DateTime.Now.AddMinutes(65), Rodzaj = Rodzaj.KoniecKursu
				},

			};
			foreach (Transakcja t in transakcje)
			{
				context.Transakcje.Add(t);
			}
			context.SaveChanges();
		}
	}
}