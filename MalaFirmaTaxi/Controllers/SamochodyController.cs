﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MalaFirmaTaxi.Data;
using MalaFirmaTaxi.Models;
using Microsoft.AspNetCore.Http;

namespace MalaFirmaTaxi.Controllers
{
	public class SamochodyController : Controller
	{
		private readonly TaxiContext _context;

		public SamochodyController(TaxiContext context)
		{
			_context = context;
		}

		// GET: Samochody
		public async Task<IActionResult> Index()
		{
			return View(await _context.Samochody.ToListAsync());
		}

		// GET: Samochody/Details/5
		public async Task<IActionResult> Details(int? id)
		{
			if (id == null)
			{
				return NotFound();
			}

			var samochod = await _context.Samochody
				.SingleOrDefaultAsync(m => m.ID == id);
			if (samochod == null)
			{
				return NotFound();
			}

			return View(samochod);
		}

		// GET: Samochody/Create
		public IActionResult Create()
		{
			return View();
		}

		// POST: Samochody/Create
		[HttpPost]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> Create([Bind("ID,Marka,Model,LiczbaMiejsc,Koszt1Km")] Samochod samochod)
		{
			if (!ModelState.IsValid) 
				return View(samochod);

			if (SamochodExists(samochod))
			{
				HttpContext.Session.SetString("AlertText", "W bazie danych istnieje już podany samochód");
				return View(samochod);
			}

			_context.Add(samochod);
			await _context.SaveChangesAsync();
			HttpContext.Session.SetString("DialogText", "Pomyślnie zapisano nowy samochód");

			if(HttpContext.Session.GetString("KierowcaID")!=null)
				return RedirectToAction("Edit", "Kierowcy", new { id = int.Parse(HttpContext.Session.GetString("KierowcaID")) });

			return RedirectToAction("Create", "Kierowcy");
		}

		// GET: Samochody/Edit/5
		public async Task<IActionResult> Edit(int? id)
		{
			if (id == null)
			{
				return NotFound();
			}

			var samochod = await _context.Samochody.SingleOrDefaultAsync(m => m.ID == id);
			if (samochod == null)
			{
				return NotFound();
			}
			return View(samochod);
		}

		// POST: Samochody/Edit/5
		[HttpPost]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> Edit(int id, [Bind("ID,Marka,Model,LiczbaMiejsc,Koszt1Km")] Samochod samochod)
		{
			if (id != samochod.ID)
			{
				return NotFound();
			}

			if (ModelState.IsValid)
			{
				try
				{
					_context.Update(samochod);
					await _context.SaveChangesAsync();
				}
				catch (DbUpdateConcurrencyException)
				{
					if (!SamochodExists(samochod.ID))
					{
						return NotFound();
					}
					else
					{
						throw;
					}
				}

				HttpContext.Session.SetString("DialogText", "Pomyślnie zapisano zmiany danych samochodu");
				if (HttpContext.Session.GetString("KierowcaID") != null)
					return RedirectToAction("Edit", "Kierowcy", new { id = int.Parse(HttpContext.Session.GetString("KierowcaID")) });

				return RedirectToAction("PanelKierowcy", "Kierowcy");
			}
			return View(samochod);
		}

		// GET: Samochody/Delete/5
		public async Task<IActionResult> Delete(int? id)
		{
			if (id == null)
			{
				return NotFound();
			}

			var samochod = await _context.Samochody
				.SingleOrDefaultAsync(m => m.ID == id);
			if (samochod == null)
			{
				return NotFound();
			}

			return View(samochod);
		}

		// POST: Samochody/Delete/5
		[HttpPost, ActionName("Delete")]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> DeleteConfirmed(int id)
		{
			var samochod = await _context.Samochody.SingleOrDefaultAsync(m => m.ID == id);
			_context.Samochody.Remove(samochod);
			await _context.SaveChangesAsync();
			return RedirectToAction("Index");
		}

		private bool SamochodExists(int id)
		{
			return _context.Samochody.Any(e => e.ID == id);
		}

		private bool SamochodExists(Samochod samochod)
		{
			return _context.Samochody.Any(e => e.Marka == samochod.Marka && e.Model == samochod.Model && e.Koszt1Km == samochod.Koszt1Km);
		}
	}
}
