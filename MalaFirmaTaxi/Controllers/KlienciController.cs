﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MalaFirmaTaxi.Data;
using MalaFirmaTaxi.Models;
using MalaFirmaTaxi.Models.RelatedViewsModels;
using Microsoft.AspNetCore.Http;

namespace MalaFirmaTaxi.Controllers
{
	public class KlienciController : Controller
	{
		private readonly TaxiContext _context;

		public KlienciController(TaxiContext context)
		{
			_context = context;
		}

		// GET: Klienci/Login
		public IActionResult Login()
		{
			return View();
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult Login(Klient klient)
		{
			var znalezionyKlient = _context.Klienci
					.FirstOrDefault(a => a.Nazwisko.Equals(klient.Nazwisko) && a.ID.Equals(klient.ID));

			if (znalezionyKlient == null)
			{
				HttpContext.Session.SetString("AlertText", "Nieprawiłowy numer klienta lub nazwisko");
				return View(klient);
			}

			HttpContext.Session.SetString("KlientID", znalezionyKlient.ID.ToString());
			HttpContext.Session.SetString("KlientFullName", znalezionyKlient.FullName);
			HttpContext.Session.SetString("DialogText", "Pomyślnie zalogowano");
			return RedirectToAction("PanelKlienta");
		}

		public async Task<IActionResult> PanelKlienta(int? zlecenieID)
		{
			if (HttpContext.Session.GetString("KlientID") != null)
			{
				var id = Int32.Parse(HttpContext.Session.GetString("KlientID"));
				var viewModel = new KlienciDetailsData
				{
					Klient = await _context.Klienci
						.Include(s => s.Zlecenia)
						.ThenInclude(k => k.Kierowca)
						.Include(s => s.Zlecenia)
						.ThenInclude(k => k.LokalizacjaStart)
						.Include(s => s.Zlecenia)
						.ThenInclude(k => k.LokalizacjaCel)
						.Include(i => i.Zlecenia)
						.ThenInclude(i => i.Transakcje)
						.ThenInclude(i => i.Lokalizacja)
						.Include(i => i.Zlecenia)
						.ThenInclude(i => i.Transakcje)
						.ThenInclude(i => i.Kierowca)
						.AsNoTracking()
						.SingleOrDefaultAsync(m => m.ID == id)
				};

				viewModel.Zlecenia = viewModel.Klient.Zlecenia;

				if (zlecenieID != null)
				{
					ViewData["ZlecenieID"] = zlecenieID.Value;
					Zlecenie zlecenie = viewModel.Zlecenia.Single(x => x.ID == zlecenieID.Value);

					viewModel.Transakcje = zlecenie.Transakcje.OrderBy(i => i.StempelCzasu);
				}

				var ostatnieZlecenieId = _context.GetNoweZlecenieKlienta(id);
				if (ostatnieZlecenieId == null)
					return View(viewModel);

				var ostatnieZlecenie = _context.FindZlecenieById(ostatnieZlecenieId.Value);
				var kierowca = _context.FindKierowcaById(ostatnieZlecenie.KierowcaID.Value);
				var samochod = _context.FindSamochodById(kierowca.SamochodID);

				var czasOczekiwania = "";
				if (HttpContext.Session.GetString("CzasOczekiwania") != null)
					czasOczekiwania = " Przewidywany czas oczekiwania: " + HttpContext.Session.GetString("CzasOczekiwania") + " minut";

				HttpContext.Session.SetString("DialogText", value: "Nasz kierowca " + kierowca.FullName +
															" przyjedzie po Ciebie samochodem " +
															samochod.Nazwa + ".\n" +
															"Koszt zlecenia: " + String.Format("{0:0.##}", ostatnieZlecenie.Koszt)
															+ czasOczekiwania);
				return View(viewModel);
			}

			return RedirectToAction("Login");
		}

		// GET: Klieci/Logout
		public IActionResult Logout()
		{
			HttpContext.Session.Clear();
			HttpContext.Session.SetString("DialogText", "Pomyślnie wylogowano");
			return Redirect("/Home");
		}

		public IActionResult CreateZlecenie()
		{
			HttpContext.Session.SetString("RedirectFromZlecenie", "/Klienci/PanelKlienta");
			return Redirect("/Zlecenia/CreateByKlient");
		}

		public IActionResult CreateAnonimowo()
		{
			return View();
		}

		// POST: Klienci/CreateAnonimowo
		[HttpPost]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> CreateAnonimowo([Bind("ID,Telefon")] Klient klient)
		{
			if (AnonimowyKlientExists(klient))
			{
				HttpContext.Session.SetString("AlertText", "Ten numer telefonu jest już używany.");
				return View(klient);
			}

			if (klient.Imie == null)
				klient.Imie = "Anonim";
			if (klient.Nazwisko == null)
				klient.Nazwisko = "Anonim";
			if (!ModelState.IsValid)
				return View(klient);

			if (KlientExists(klient))
			{
				HttpContext.Session.SetString("KlientID", _context.FindKlientByTelefon(klient.Telefon).ID.ToString());
				HttpContext.Session.SetString("KlientFullName", klient.Nazwisko);
				return RedirectToAction("CreateByKlient", "Zlecenia");
			}
			_context.Add(klient);
			await _context.SaveChangesAsync();
			HttpContext.Session.SetString("KlientID", klient.ID.ToString());
			HttpContext.Session.SetString("KlientFullName", klient.Nazwisko);
			return RedirectToAction("CreateByKlient", "Zlecenia");
		}

		// GET: Klienci
		public async Task<IActionResult> Index(int? id)
		{
			var klienci = _context.Klienci
				.AsNoTracking()
				.OrderBy(i => i.ID)
				.ToListAsync();

			return View(await klienci);
		}

		// GET: Klienci/Details/5
		public async Task<IActionResult> Details(int? id, int? zlecenieID)
		{
			if (id == null)
			{
				return NotFound();
			}

			var viewModel = new KlienciDetailsData
			{
				Klient = await _context.Klienci
					.Include(s => s.Zlecenia)
					.ThenInclude(k => k.Kierowca)
					.Include(s => s.Zlecenia)
					.ThenInclude(k => k.LokalizacjaStart)
					.Include(s => s.Zlecenia)
					.ThenInclude(k => k.LokalizacjaCel)
					.Include(i => i.Zlecenia)
					.ThenInclude(i => i.Transakcje)
					.ThenInclude(i => i.Lokalizacja)
					.Include(i => i.Zlecenia)
					.ThenInclude(i => i.Transakcje)
					.ThenInclude(i => i.Kierowca)
					.AsNoTracking()
					.SingleOrDefaultAsync(m => m.ID == id)
			};


			viewModel.Zlecenia = viewModel.Klient.Zlecenia;

			if (zlecenieID != null)
			{
				ViewData["ZlecenieID"] = zlecenieID.Value;
				Zlecenie zlecenie = viewModel.Zlecenia.Single(x => x.ID == zlecenieID.Value);

				viewModel.Transakcje = zlecenie.Transakcje.OrderBy(i => i.StempelCzasu);
			}

			return View(viewModel);
		}

		// GET: Klienci/Create
		public IActionResult Create()
		{
			return View();
		}

		// POST: Klienci/Create
		[HttpPost]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> Create([Bind("ID,Imie,Nazwisko,Telefon")] Klient klient)
		{
			if (klient.Nazwisko == null)
			{
				HttpContext.Session.SetString("AlertText", "Należy podać nazwisko!");
				return View(klient);
			}

			if (!ModelState.IsValid)
				return View(klient);

			if (KlientExists(klient))
			{
				HttpContext.Session.SetString("AlertText", "W bazie danych istnieje już klient o podanych danych!");
				return View(klient);
			}

			_context.Add(klient);
			await _context.SaveChangesAsync();
			HttpContext.Session.SetString("DialogText", "Możesz się teraz zalogować. Twój numer klienta to: " + klient.ID.ToString());
			return RedirectToAction("PanelKlienta");
		}

		// GET: Klienci/Edit/5
		public async Task<IActionResult> Edit(int? id)
		{
			if (id == null)
			{
				return NotFound();
			}

			var klient = await _context.Klienci.SingleOrDefaultAsync(m => m.ID == id);
			if (klient == null)
			{
				return NotFound();
			}
			return View(klient);
		}

		// POST: Klienci/Edit/5
		[HttpPost]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> Edit(int id, [Bind("ID,Imie,Nazwisko,Telefon")] Klient klient)
		{
			if (id != klient.ID)
			{
				return NotFound();
			}

			if (ModelState.IsValid)
			{
				try
				{
					_context.Update(klient);
					await _context.SaveChangesAsync();
				}
				catch (DbUpdateConcurrencyException)
				{
					if (!KlientExists(klient.ID))
					{
						return NotFound();
					}
					else
					{
						throw;
					}
				}
				HttpContext.Session.SetString("DialogText", "Pomyślnie zapisano zmiany danych");
				
				return RedirectToAction("PanelKlienta");
			}
			return View(klient);
		}

		// GET: Klienci/Delete/5
		public async Task<IActionResult> Delete(int? id)
		{
			if (id == null)
			{
				return NotFound();
			}

			var klient = await _context.Klienci
				.SingleOrDefaultAsync(m => m.ID == id);
			if (klient == null)
			{
				return NotFound();
			}

			return View(klient);
		}

		// POST: Klienci/Delete/5
		[HttpPost, ActionName("Delete")]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> DeleteConfirmed(int id)
		{
			var klient = await _context.Klienci.SingleOrDefaultAsync(m => m.ID == id);
			_context.Klienci.Remove(klient);
			await _context.SaveChangesAsync();
			return RedirectToAction("Index");
		}

		private bool KlientExists(int id)
		{
			return _context.Klienci.Any(e => e.ID == id);
		}

		private bool KlientExists(Klient klient)
		{
			return _context.Klienci.Any(e => e.Imie == klient.Imie && e.Nazwisko == klient.Nazwisko && e.Telefon == klient.Telefon);
		}

		private bool AnonimowyKlientExists(Klient klient)
		{
			return _context.Klienci.Any(e => e.Telefon == klient.Telefon);
		}
	}
}
