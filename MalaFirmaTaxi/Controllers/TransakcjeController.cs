﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MalaFirmaTaxi.Data;
using MalaFirmaTaxi.Models;
using Microsoft.AspNetCore.Http;

namespace MalaFirmaTaxi.Controllers
{
    public class TransakcjeController : Controller
    {
        private readonly TaxiContext _context;

        public TransakcjeController(TaxiContext context)
        {
            _context = context;    
        }

        // GET: Transakcje
        public async Task<IActionResult> Index()
        {
            var taxiContext = _context.Transakcje.Include(t => t.Kierowca).Include(t => t.Lokalizacja).Include(t => t.Zlecenie);
            return View(await taxiContext.ToListAsync());
        }

        // GET: Transakcje/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var transakcja = await _context.Transakcje
                .Include(t => t.Kierowca)
                .Include(t => t.Lokalizacja)
                .Include(t => t.Zlecenie)
                .SingleOrDefaultAsync(m => m.ID == id);

            if (transakcja == null)
            {
                return NotFound();
            }

            return View(transakcja);
        }

        // GET: Transakcje/Create
        public IActionResult Create()
        {
            ViewData["KierowcaID"] = new SelectList(_context.Kierowcy, "ID", "FullName");
	        ViewData["LokalizacjaID"] = PopulateLokalizacjaDropDownListOrderedByUlica();

			ViewData["ZlecenieID"] = new SelectList(_context.Zlecenia, "ID", "ID");
            return View();
        }

        // POST: Transakcje/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ID,KierowcaID,LokalizacjaID,ZlecenieID,StempelCzasu,Rodzaj,Uwagi")] Transakcja transakcja)
        {
            if (ModelState.IsValid)
            {
                _context.Add(transakcja);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewData["KierowcaID"] = new SelectList(_context.Kierowcy, "ID", "Imie", transakcja.KierowcaID);
            ViewData["LokalizacjaID"] = new SelectList(_context.Lokalizacje, "ID", "Adres", transakcja.LokalizacjaID);
            ViewData["ZlecenieID"] = new SelectList(_context.Zlecenia, "ID", "ID", transakcja.ZlecenieID);
            return View(transakcja);
        }

		// GET: Transakcje/CreateByKierowca
		public IActionResult CreateByKierowca()
		{
			if (HttpContext.Session.GetString("RedirectFromTransakcja") == null)
				HttpContext.Session.SetString("RedirectFromTransakcja", "/Kierowcy/PanelKierowcy");

			if (HttpContext.Session.GetString("KierowcaID") == null || HttpContext.Session.GetString("KierowcaFullName") == null)
			{
				return Redirect("/Kierowcy/Login");
			}

			HttpContext.Session.SetString("RedirectFromLokalizacja", "/Transakcje/CreateByKierowca");
			ViewData["LokalizacjaID"] = new SelectList(_context.Lokalizacje, "ID", "Adres");
		    ViewData["ZlecenieID"] = new SelectList(_context.Zlecenia, "ID", "ID");
		    return View();
	    }

		// POST: Transakcje/CreateByKierowca
		[HttpPost]
	    [ValidateAntiForgeryToken]
	    public async Task<IActionResult> CreateByKierowca([Bind("ID,KierowcaID,LokalizacjaID,ZlecenieID,StempelCzasu,Rodzaj,Uwagi")] Transakcja transakcja)
	    {
		    if (ModelState.IsValid)
		    {
			    _context.Add(transakcja);
			    await _context.SaveChangesAsync();
			    HttpContext.Session.SetString("DialogText", "Utworzono nową transakcję");
				return Redirect(HttpContext.Session.GetString("RedirectFromTransakcja"));
		    }

			ViewData["LokalizacjaID"] = new SelectList(_context.Lokalizacje, "ID", "Adres", transakcja.LokalizacjaID);
		    ViewData["ZlecenieID"] = new SelectList(_context.Zlecenia, "ID", "ID", transakcja.ZlecenieID);
		    return View(transakcja);
	    }

		// GET: Transakcje/CreateAwariaByKierowca
		public IActionResult CreateAwariaByKierowca()
	    {
		    if (HttpContext.Session.GetString("RedirectFromTransakcja") == null)
			    HttpContext.Session.SetString("RedirectFromTransakcja", "/Kierowcy/PanelKierowcy");

		    if (HttpContext.Session.GetString("KierowcaID") == null)
		    {
			    return Redirect("/Kierowcy/PanelKierowcy");
		    }

		    HttpContext.Session.SetString("RedirectFromLokalizacja", "/Transakcje/CreateAwariaByKierowca");
		    ViewData["KierowcaID"] = HttpContext.Session.GetString("KierowcaID");
		    ViewData["LokalizacjaID"] = PopulateLokalizacjaDropDownListOrderedByUlica();
			ViewData["ZlecenieID"] = HttpContext.Session.GetString("ZlecenieID");

			return View();
	    }

		// POST: Transakcje/CreateAwariaByKierowca
		[HttpPost]
	    [ValidateAntiForgeryToken]
	    public async Task<IActionResult> CreateAwariaByKierowca([Bind("ID,KierowcaID,LokalizacjaID,ZlecenieID,StempelCzasu,Rodzaj,Uwagi")] Transakcja transakcja)
	    {
		    if (ModelState.IsValid)
		    {
			    _context.Add(transakcja);
			    await _context.SaveChangesAsync();
			    HttpContext.Session.SetString("DialogText", "Zarejestrowano traksakcję: Awaria. Zaczekaj z pasażerem do czasu przyjazdu kierowcy zastępczego.");
				_context.AddAwaryjnePrzyjecieZleceniaForZlecenie(transakcja.ZlecenieID.Value);
			    return Redirect(HttpContext.Session.GetString("RedirectFromTransakcja"));
		    }

			ViewData["KierowcaID"] = HttpContext.Session.GetString("KierowcaID");
		    ViewData["LokalizacjaID"] = new SelectList(_context.Lokalizacje, "ID", "Adres");
		    ViewData["ZlecenieID"] = HttpContext.Session.GetString("ZlecenieID");
			return View(transakcja);
	    }

	    public IActionResult CreateAktywnyByKierowca()
		{
			if (HttpContext.Session.GetString("KierowcaID") == null)
			{
				return RedirectToAction("PanelKierowcy", "Kierowcy");
			}

			HttpContext.Session.SetString("RedirectFromLokalizacja", "/Transakcje/CreateAktywnyByKierowca");
			ViewData["KierowcaID"] = HttpContext.Session.GetString("KierowcaID");
			ViewData["LokalizacjaID"] = PopulateLokalizacjaDropDownListOrderedByUlica();
			ViewData["ZlecenieID"] = HttpContext.Session.GetString("ZlecenieID");

			return View();
		}

		private SelectList PopulateLokalizacjaDropDownListOrderedByUlica()
		{
			return new SelectList(_context.Lokalizacje.OrderBy(s => s.Ulica), "ID", "Adres");
		}

		[HttpPost]
	    [ValidateAntiForgeryToken]
	    public async Task<IActionResult> CreateAktywnyByKierowca([Bind("ID,KierowcaID,LokalizacjaID,ZlecenieID,StempelCzasu,Rodzaj,Uwagi")] Transakcja transakcja)
	    {
		    if (ModelState.IsValid)
		    {
			    _context.Add(transakcja);
			    await _context.SaveChangesAsync();
			    HttpContext.Session.SetString("DialogText", "Zarejestrowano traksakcję: Aktywny. Witamy na pokładzie!");
			    return RedirectToAction("PanelKierowcy", "Kierowcy");
		    }

		    ViewData["KierowcaID"] = HttpContext.Session.GetString("KierowcaID");
		    ViewData["LokalizacjaID"] = new SelectList(_context.Lokalizacje, "ID", "Adres");
		    ViewData["ZlecenieID"] = HttpContext.Session.GetString("ZlecenieID");
		    return View(transakcja);
	    }

	    public IActionResult CreateNieaktywnyByKierowca()
	    {
		    if (HttpContext.Session.GetString("KierowcaID") == null)
		    {
			    return RedirectToAction("PanelKierowcy", "Kierowcy");
		    }

		    HttpContext.Session.SetString("RedirectFromLokalizacja", "/Transakcje/CreateNieaktywnyByKierowca");
		    ViewData["KierowcaID"] = HttpContext.Session.GetString("KierowcaID");
		    ViewData["LokalizacjaID"] = PopulateLokalizacjaDropDownListOrderedByUlica();
		    ViewData["ZlecenieID"] = HttpContext.Session.GetString("ZlecenieID");

		    return View();
	    }

	    [HttpPost]
	    [ValidateAntiForgeryToken]
	    public async Task<IActionResult> CreateNieaktywnyByKierowca([Bind("ID,KierowcaID,LokalizacjaID,ZlecenieID,StempelCzasu,Rodzaj,Uwagi")] Transakcja transakcja)
	    {
		    if (ModelState.IsValid)
		    {
			    _context.Add(transakcja);
			    await _context.SaveChangesAsync();
			    HttpContext.Session.SetString("DialogText", "Zarejestrowano traksakcję: Nieaktywny. Załatw swoje sprawy i szybko wracaj do nas! ");
				return RedirectToAction("PanelKierowcy", "Kierowcy");
			}

		    ViewData["KierowcaID"] = HttpContext.Session.GetString("KierowcaID");
		    ViewData["LokalizacjaID"] = new SelectList(_context.Lokalizacje, "ID", "Adres");
		    ViewData["ZlecenieID"] = HttpContext.Session.GetString("ZlecenieID");
		    return View(transakcja);
	    }

		// GET: Transakcje/Edit/5
		public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var transakcja = await _context.Transakcje.SingleOrDefaultAsync(m => m.ID == id);
            if (transakcja == null)
            {
                return NotFound();
            }
            ViewData["KierowcaID"] = new SelectList(_context.Kierowcy, "ID", "Imie", transakcja.KierowcaID);
            ViewData["LokalizacjaID"] = new SelectList(_context.Lokalizacje, "ID", "Miasto", transakcja.LokalizacjaID);
            ViewData["ZlecenieID"] = new SelectList(_context.Zlecenia, "ID", "ID", transakcja.ZlecenieID);
            return View(transakcja);
        }

        // POST: Transakcje/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ID,KierowcaID,LokalizacjaID,ZlecenieID,StempelCzasu,Rodzaj,Uwagi")] Transakcja transakcja)
        {
            if (id != transakcja.ID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(transakcja);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!TransakcjaExists(transakcja.ID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Index");
            }
            ViewData["KierowcaID"] = new SelectList(_context.Kierowcy, "ID", "Imie", transakcja.KierowcaID);
            ViewData["LokalizacjaID"] = new SelectList(_context.Lokalizacje, "ID", "Miasto", transakcja.LokalizacjaID);
            ViewData["ZlecenieID"] = new SelectList(_context.Zlecenia, "ID", "ID", transakcja.ZlecenieID);
            return View(transakcja);
        }

        // GET: Transakcje/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var transakcja = await _context.Transakcje
                .Include(t => t.Kierowca)
                .Include(t => t.Lokalizacja)
                .Include(t => t.Zlecenie)
                .SingleOrDefaultAsync(m => m.ID == id);
            if (transakcja == null)
            {
                return NotFound();
            }

            return View(transakcja);
        }

        // POST: Transakcje/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var transakcja = await _context.Transakcje.SingleOrDefaultAsync(m => m.ID == id);
            _context.Transakcje.Remove(transakcja);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        private bool TransakcjaExists(int id)
        {
            return _context.Transakcje.Any(e => e.ID == id);
        }
    }
}
