﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MalaFirmaTaxi.Data;
using MalaFirmaTaxi.Models;
using MalaFirmaTaxi.Models.RelatedViewsModels;
using Microsoft.AspNetCore.Http;

namespace MalaFirmaTaxi.Controllers
{
	public class KierowcyController : Controller
	{
		private readonly TaxiContext _context;

		public KierowcyController(TaxiContext context)
		{
			_context = context;
		}

		// GET: Kierowcy/Login
		public IActionResult Login()
		{
			return View();
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult Login(Kierowca kierowca)
		{
			var znalezionyKierowca = _context.Kierowcy
				.FirstOrDefault(a => a.Nazwisko.Equals(kierowca.Nazwisko) && a.ID.Equals(kierowca.ID));

			if (znalezionyKierowca == null)
			{
				HttpContext.Session.SetString("AlertText", "Nieprawiłowy numer kierowcy lub nazwisko");
				return View(kierowca);
			}

			HttpContext.Session.SetString("KierowcaID", znalezionyKierowca.ID.ToString());
			HttpContext.Session.SetString("KierowcaFullName", znalezionyKierowca.FullName);
			HttpContext.Session.SetString("DialogText", "Pomyślnie zalogowano");
			return RedirectToAction("PanelKierowcy");
		}

		public async Task<IActionResult> PanelKierowcy(int? zlecenieId)
		{
			if (HttpContext.Session.GetString("KierowcaID") == null)
				return RedirectToAction("Login");

			var id = Int32.Parse(HttpContext.Session.GetString("KierowcaID"));
			var viewModel = new KierowcyDetailsData
			{
				Kierowca = await _context.Kierowcy
					.Include(k => k.Samochod)
					.Include(s => s.Zlecenia)
						.ThenInclude(k => k.Klient)
					.Include(s => s.Zlecenia)
						.ThenInclude(k => k.LokalizacjaStart)
					.Include(s => s.Zlecenia)
						.ThenInclude(k => k.LokalizacjaCel)
					.Include(i => i.Zlecenia)
						.ThenInclude(i => i.Transakcje)
						.ThenInclude(i => i.Lokalizacja)
					.Include(i => i.Zlecenia)
						.ThenInclude(i => i.Transakcje)
						.ThenInclude(i => i.Kierowca)
					.Include(i => i.Transakcje)
						.ThenInclude(i => i.Lokalizacja)
					.AsNoTracking()
					.SingleOrDefaultAsync(m => m.ID == id)
			};

			viewModel.Zlecenia = viewModel.Kierowca.Zlecenia;
			if (!viewModel.Zlecenia.Any())
				viewModel.Zlecenia = null;

			viewModel.TransakcjeBezZlecenia = viewModel.Kierowca.Transakcje
				.Where(s => s.ZlecenieID == null)
				.OrderBy(i => i.StempelCzasu); 

			if (!viewModel.TransakcjeBezZlecenia.Any())
				viewModel.TransakcjeBezZlecenia = null;

			if (zlecenieId != null)
			{
				ViewData["ZlecenieID"] = zlecenieId.Value;
				var zlecenie = viewModel.Zlecenia.Single(x => x.ID == zlecenieId.Value);

				viewModel.Transakcje = zlecenie.Transakcje.OrderBy(i => i.StempelCzasu);
			}

			if (_context.AwariaLubNieaktywnyKierowca(id))
			{
				HttpContext.Session.SetString("AlertText", "Wykryliśmy, że jesteś nieaktywny lub w stanie awarii. " +
				                                           "Postaraj się szybko wrócić do pracy.");
			}

			var awaryjneZlecenieId = _context.CheckForAwaryjneZlecenieForKierowca(id);
			if (awaryjneZlecenieId != null)
			{
				var awaryjneZlecenie = _context.FindZlecenieById(awaryjneZlecenieId.Value);
				awaryjneZlecenie.Klient = _context.FindKlientById(awaryjneZlecenie.KlientID);
				awaryjneZlecenie.LokalizacjaStart = _context.FindAwaryjnaLokalizacjaOfZlecenie(awaryjneZlecenieId);
				awaryjneZlecenie.LokalizacjaCel = _context.FindLokalizacjaById(awaryjneZlecenie.LokalizacjaCelID);
				viewModel.AwaryjneZlecenie = awaryjneZlecenie;
			}
			
			var ostatnieZlecenieId = _context.CheckForRecentZlecenieForKierowca(id);
			if (ostatnieZlecenieId == null)
				return View(viewModel);

			var ostatnieZlecenie = _context.FindZlecenieById(ostatnieZlecenieId.Value);
			ostatnieZlecenie.Klient = _context.FindKlientById(ostatnieZlecenie.KlientID);
			ostatnieZlecenie.LokalizacjaStart = _context.FindLokalizacjaById(ostatnieZlecenie.LokalizacjaStartID);
			ostatnieZlecenie.LokalizacjaCel = _context.FindLokalizacjaById(ostatnieZlecenie.LokalizacjaCelID);
			viewModel.NoweZlecenie = ostatnieZlecenie;
			if (_context.AwaryjnaTransakcjaWZleceniu(ostatnieZlecenieId.Value))
			{
				viewModel.JestAwariaNowegoZlecenia = true;
			}

			return View(viewModel);
		}

		// GET: Kierowcy/Logout
		public IActionResult Logout()
		{
			HttpContext.Session.Clear();
			HttpContext.Session.SetString("DialogText", "Pomyślnie wylogowano");
			return Redirect("/Home");
		}

		public IActionResult CreateTransakcja()
		{
			HttpContext.Session.SetString("RedirectFromTransakcja", "/Kierowcy/PanelKierowcy");
			return Redirect("/Transakcje/CreateByKierowca");
		}

		public async Task<IActionResult> RegisterOdbiorPasazeraForZlecenie(int zlecenieId)
		{
			var zlecenie = _context.FindZlecenieById(zlecenieId);
			var odbiorPasazera = new Transakcja
			{
				ZlecenieID = zlecenie.ID,
				KierowcaID = zlecenie.KierowcaID.Value,
				LokalizacjaID = zlecenie.LokalizacjaStartID,
				Rodzaj = Rodzaj.OdbiorPasazera,
				Uwagi = "Automatyczna transakcja"
			};

			if (_context.TransakcjaAlreadyExists(odbiorPasazera))
			{
				HttpContext.Session.SetString("AlertText", "Taka transakcja już istnieje");
				return Redirect("PanelKierowcy");
			}

			_context.Add(odbiorPasazera);
			await _context.SaveChangesAsync();

			HttpContext.Session.SetString("DialogText", "Zarejestrowano traksakcję: Odbiór pasażera | " + odbiorPasazera.StempelCzasu);
			return Redirect("PanelKierowcy");
		}

		public async Task<IActionResult> RegisterKoniecKursuForZlecenie(int zlecenieId)
		{
			if (!_context.OdebranoPasazeraWZleceniu(zlecenieId) || _context.AwaryjnaTransakcjaWZleceniu(zlecenieId))
			{
				HttpContext.Session.SetString("AlertText", "Nie można zarejestrować tej transakcji");
				return Redirect("PanelKierowcy");
			}

			var zlecenie = _context.FindZlecenieById(zlecenieId);
			var koniecKursu = new Transakcja
			{
				ZlecenieID = zlecenie.ID,
				KierowcaID = zlecenie.KierowcaID.Value,
				LokalizacjaID = zlecenie.LokalizacjaCelID,
				Rodzaj = Rodzaj.KoniecKursu,
				Uwagi = "Automatyczna transakcja"
			};

			if (_context.TransakcjaAlreadyExists(koniecKursu))
			{
				HttpContext.Session.SetString("AlertText", "Taka transakcja już istnieje");
				return Redirect("PanelKierowcy");
			}

			_context.Add(koniecKursu);
			await _context.SaveChangesAsync();

			HttpContext.Session.SetString("DialogText", "Zarejestrowano traksakcję: Koniec kursu | " + koniecKursu.StempelCzasu);
			return Redirect("PanelKierowcy");
		}

		public async Task<IActionResult> RegisterAwaryjnyKoniecKursuForZlecenie(int zlecenieId)
		{
			if (HttpContext.Session.GetString("KierowcaID") == null)
				return RedirectToAction("PanelKierowcy");

			if (!_context.OdebranoAwaryjniePasazeraWZleceniu(zlecenieId))
			{
				HttpContext.Session.SetString("AlertText", "Nie można zarejestrować tej transakcji");
				return Redirect("PanelKierowcy");
			}

			var zlecenie = _context.FindZlecenieById(zlecenieId);
			var koniecKursu = new Transakcja
			{
				ZlecenieID = zlecenie.ID,
				KierowcaID = int.Parse(HttpContext.Session.GetString("KierowcaID")),
				LokalizacjaID = zlecenie.LokalizacjaCelID,
				Rodzaj = Rodzaj.KoniecKursu,
				Uwagi = "Automatyczna transakcja"
			};

			if (_context.TransakcjaAlreadyExists(koniecKursu))
			{
				HttpContext.Session.SetString("AlertText", "Taka transakcja już istnieje");
				return Redirect("PanelKierowcy");
			}

			_context.Add(koniecKursu);
			await _context.SaveChangesAsync();

			HttpContext.Session.SetString("DialogText", "Zarejestrowano traksakcję: Koniec kursu  | " + koniecKursu.StempelCzasu);
			return Redirect("PanelKierowcy");
		}

		public async Task<IActionResult> RegisterPrzejeciePasazeraPoAwariiForZlecenie(int zlecenieId)
		{
			if (HttpContext.Session.GetString("KierowcaID") == null)
				return RedirectToAction("PanelKierowcy");
			var zlecenie = _context.FindZlecenieById(zlecenieId);
			var przejeciePasazera = new Transakcja
			{
				ZlecenieID = zlecenie.ID,
				KierowcaID = int.Parse(HttpContext.Session.GetString("KierowcaID")),
				LokalizacjaID = _context.FindAwaryjnaLokalizacjaIdOfZlecenie(zlecenieId),
				Rodzaj = Rodzaj.PrzejeciePasazeraPoAwarii,
				Uwagi = "Automatyczna transakcja"
			};

			if (_context.TransakcjaAlreadyExists(przejeciePasazera))
			{
				HttpContext.Session.SetString("AlertText", "Taka transakcja już istnieje");
				return Redirect("PanelKierowcy");
			}

			_context.Add(przejeciePasazera);
			await _context.SaveChangesAsync();
			HttpContext.Session.SetString("DialogText", "Zarejestrowano traksakcję: Przejęcie pasażera po awarii | " + przejeciePasazera.StempelCzasu);

			return Redirect("PanelKierowcy");
		}

		public IActionResult RegisterAwariaForZlecenie(int zlecenieId)
		{
			if (HttpContext.Session.GetString("KierowcaID") == null)
				return RedirectToAction("PanelKierowcy");

			var kierowcaId = int.Parse(HttpContext.Session.GetString("KierowcaID"));
			if (_context.AwaryjnaTransakcjaKierowcyWZleceniu(zlecenieId, kierowcaId))
			{
				HttpContext.Session.SetString("AlertText", "Taka transakcja już istnieje");
				return Redirect("PanelKierowcy");
			}

			HttpContext.Session.SetString("ZlecenieID", zlecenieId.ToString());
			HttpContext.Session.SetString("RedirectFromTransakcja", "/Kierowcy/PanelKierowcy");
			return Redirect("/Transakcje/CreateAwariaByKierowca");
		}

		// GET: Kierowcy
		public async Task<IActionResult> Index()
		{
			var kierowcy = _context.Kierowcy
				.Include(z => z.Samochod)
				.AsNoTracking()
				.OrderBy(i => i.ID)
				.ToListAsync();

			return View(await kierowcy);
		}

		// GET: Kierowcy/Details/5
		public async Task<IActionResult> Details(int? id, int? zlecenieId)
		{
			if (id == null)
			{
				return NotFound();
			}

			var viewModel = new KierowcyDetailsData
			{
				Kierowca = await _context.Kierowcy
					.Include(k => k.Samochod)
					.Include(s => s.Zlecenia)
					.ThenInclude(k => k.Klient)
					.Include(s => s.Zlecenia)
					.ThenInclude(k => k.LokalizacjaStart)
					.Include(s => s.Zlecenia)
					.ThenInclude(k => k.LokalizacjaCel)
					.Include(i => i.Zlecenia)
					.ThenInclude(i => i.Transakcje)
					.ThenInclude(i => i.Lokalizacja)
					.Include(i => i.Zlecenia)
					.ThenInclude(i => i.Transakcje)
					.ThenInclude(i => i.Kierowca)
					.AsNoTracking()
					.SingleOrDefaultAsync(m => m.ID == id)
			};

			viewModel.Zlecenia = viewModel.Kierowca.Zlecenia;

			if (zlecenieId != null)
			{
				ViewData["ZlecenieID"] = zlecenieId.Value;
				Zlecenie zlecenie = viewModel.Zlecenia.Single(x => x.ID == zlecenieId.Value);

				viewModel.Transakcje = zlecenie.Transakcje.OrderBy(i => i.StempelCzasu);
			}

			return View(viewModel);
		}

		// GET: Kierowcy/Create
		public IActionResult Create()
		{
			HttpContext.Session.SetString("RedirectFromSamochodCreate", "/Kierowcy/Create");
			ViewData["SamochodID"] = new SelectList(PopulateSamochodDropDownList(), "ID", "Nazwa");
			return View();
		}

		private IEnumerable<Samochod> PopulateSamochodDropDownList()
		{
			return _context.Samochody.Where(s => s.Kierowca == null).OrderBy(s => s.Marka);
		}

		// POST: Kierowcy/Create
		[HttpPost]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> Create([Bind("ID,Imie,Nazwisko,SamochodID")] Kierowca kierowca)
		{
			if (KierowcaExists(kierowca))
			{
				HttpContext.Session.SetString("AlertText", "W bazie danych istnieje już kierowca o podanych danych!");
				return RedirectToAction("Create");
			}

			if (kierowca.Nazwisko == null)
			{
				HttpContext.Session.SetString("AlertText", "Należy podać nazwisko!");
				ViewData["SamochodID"] = new SelectList(_context.Samochody.OrderBy(s => s.Marka), "ID", "Nazwa", kierowca.SamochodID);
				return View(kierowca);
			}

			if (!ModelState.IsValid)
			{
				ViewData["SamochodID"] = new SelectList(_context.Samochody.OrderBy(s => s.Marka), "ID", "Nazwa", kierowca.SamochodID);
				return View(kierowca);
			}
			
			_context.Add(kierowca);
			await _context.SaveChangesAsync();

			HttpContext.Session.SetString("DialogText", "Możesz się teraz zalogować. Twój numer kierowcy to: " + kierowca.ID.ToString());
			return RedirectToAction("PanelKierowcy");
		}

		// GET: Kierowcy/Edit/5
		public async Task<IActionResult> Edit(int? id)
		{
			if (id == null)
			{
				return NotFound();
			}

			HttpContext.Session.SetString("RedirectFromSamochodCreate", id.ToString());
			HttpContext.Session.SetString("RedirectFromSamochodEdit", id.ToString());

			var kierowca = await _context.Kierowcy.SingleOrDefaultAsync(m => m.ID == id);
			if (kierowca == null)
			{
				return NotFound();
			}
			ViewData["SamochodID"] = new SelectList(PopulateSamochodDropDownListInEditView(id.Value), "ID", "Nazwa", kierowca.SamochodID);
			return View(kierowca);
		}

		private IEnumerable<Samochod> PopulateSamochodDropDownListInEditView(int kierowcaId)
		{
			return _context.Samochody.Where(s => s.Kierowca == null || s.Kierowca.ID == kierowcaId).OrderBy(s => s.Marka);
		}

		// POST: Kierowcy/Edit/5
		[HttpPost]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> Edit(int id, [Bind("ID,Imie,Nazwisko,SamochodID")] Kierowca kierowca)
		{
			if (id != kierowca.ID)
			{
				return NotFound();
			}

			if (ModelState.IsValid)
			{
				try
				{
					_context.Update(kierowca);
					await _context.SaveChangesAsync();
					HttpContext.Session.SetString("DialogText", "Pomyślnie zapisano zmiany danych.");
				}
				catch (DbUpdateConcurrencyException)
				{
					if (!KierowcaExists(kierowca.ID))
					{
						return NotFound();
					}
					else
					{
						throw;
					}
				}

				return RedirectToAction("PanelKierowcy");
			}
			ViewData["SamochodID"] = new SelectList(PopulateSamochodDropDownListInEditView(id), "ID", "Marka", kierowca.SamochodID);
			return View(kierowca);
		}

		// GET: Kierowcy/Delete/5
		public async Task<IActionResult> Delete(int? id)
		{
			if (id == null)
			{
				return NotFound();
			}

			var kierowca = await _context.Kierowcy
				.Include(k => k.Samochod)
				.SingleOrDefaultAsync(m => m.ID == id);
			if (kierowca == null)
			{
				return NotFound();
			}

			return View(kierowca);
		}

		// POST: Kierowcy/Delete/5
		[HttpPost, ActionName("Delete")]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> DeleteConfirmed(int id)
		{
			var kierowca = await _context.Kierowcy.SingleOrDefaultAsync(m => m.ID == id);
			_context.Kierowcy.Remove(kierowca);
			await _context.SaveChangesAsync();
			return RedirectToAction("Index");
		}

		private bool KierowcaExists(int id)
		{
			return _context.Kierowcy.Any(e => e.ID == id);
		}

		private bool KierowcaExists(Kierowca kierowca)
		{
			return _context.Kierowcy.Any(e => e.FullName == kierowca.FullName && e.SamochodID == kierowca.SamochodID);
		}

	}
}
