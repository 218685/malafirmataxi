﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MalaFirmaTaxi.Data;
using MalaFirmaTaxi.Models;
using Microsoft.AspNetCore.Http;

namespace MalaFirmaTaxi.Controllers
{
    public class LokalizacjeController : Controller
    {
        private readonly TaxiContext _context;

        public LokalizacjeController(TaxiContext context)
        {
            _context = context;    
        }

        // GET: Lokalizacjas
        public async Task<IActionResult> Index()
        {
            return View(await _context.Lokalizacje.ToListAsync());
        }

        // GET: Lokalizacjas/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var lokalizacja = await _context.Lokalizacje
                .SingleOrDefaultAsync(m => m.ID == id);
            if (lokalizacja == null)
            {
                return NotFound();
            }

            return View(lokalizacja);
        }

        // GET: Lokalizacjas/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Lokalizacjas/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ID,Ulica,Numer,Miasto,DlugoscGeogr,SzerokoscGeogr")] Lokalizacja lokalizacja)
        {
            if (ModelState.IsValid)
            {
	            if (LokalizacjaExists(lokalizacja))
	            {
		            HttpContext.Session.SetString("AlertText", "W bazie danych istnieje już podana lokalizacja");
		            return View(lokalizacja);
	            }
				_context.Add(lokalizacja);
                await _context.SaveChangesAsync();
				//return RedirectToAction("Create","Zlecenia");
				HttpContext.Session.SetString("DialogText", "Dodano nową lokalizację");
				return Redirect(HttpContext.Session.GetString("RedirectFromLokalizacja"));
            }
            return View(lokalizacja);
        }

        // GET: Lokalizacjas/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var lokalizacja = await _context.Lokalizacje.SingleOrDefaultAsync(m => m.ID == id);
            if (lokalizacja == null)
            {
                return NotFound();
            }
            return View(lokalizacja);
        }

        // POST: Lokalizacjas/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ID,Ulica,Numer,Miasto,DlugoscGeogr,SzerokoscGeogr")] Lokalizacja lokalizacja)
        {
            if (id != lokalizacja.ID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(lokalizacja);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!LokalizacjaExists(lokalizacja.ID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Index");
            }
            return View(lokalizacja);
        }

        // GET: Lokalizacjas/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var lokalizacja = await _context.Lokalizacje
                .SingleOrDefaultAsync(m => m.ID == id);
            if (lokalizacja == null)
            {
                return NotFound();
            }

            return View(lokalizacja);
        }

        // POST: Lokalizacjas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var lokalizacja = await _context.Lokalizacje.SingleOrDefaultAsync(m => m.ID == id);
            _context.Lokalizacje.Remove(lokalizacja);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        private bool LokalizacjaExists(int id)
        {
            return _context.Lokalizacje.Any(e => e.ID == id);
        }

	    private bool LokalizacjaExists(Lokalizacja lokalizacja)
	    {
		    return _context.Lokalizacje.Any(e => e.Ulica == lokalizacja.Ulica && e.Numer == lokalizacja.Numer);
	    }
	}
}
