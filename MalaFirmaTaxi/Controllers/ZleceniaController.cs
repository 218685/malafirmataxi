﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MalaFirmaTaxi.Data;
using MalaFirmaTaxi.Models;
using Microsoft.AspNetCore.Http;

namespace MalaFirmaTaxi.Controllers
{
	public class ZleceniaController : Controller
	{
		private readonly TaxiContext _context;

		public ZleceniaController(TaxiContext context)
		{
			_context = context;
		}

		// GET: Zlecenia
		public async Task<IActionResult> Index()
		{
			var taxiContext = _context.Zlecenia
				.Include(z => z.Kierowca)
				.Include(z => z.Klient)
				.Include(z => z.LokalizacjaCel)
				.Include(z => z.LokalizacjaStart);

			return View(await taxiContext.ToListAsync());
		}

		// GET: Zlecenia/Details/5
		public async Task<IActionResult> Details(int? id)
		{
			if (id == null)
			{
				return NotFound();
			}

			var zlecenie = await _context.Zlecenia
				.Include(z => z.Kierowca)
				.Include(z => z.Klient)
				.Include(z => z.LokalizacjaCel)
				.Include(z => z.LokalizacjaStart)
				.Include(z => z.Transakcje)
					.ThenInclude(i => i.Lokalizacja)
				.Include(z => z.Transakcje)
					.ThenInclude(i => i.Kierowca)
				.SingleOrDefaultAsync(m => m.ID == id);

			if (zlecenie == null)
			{
				return NotFound();
			}

			return View(zlecenie);
		}

		// GET: Zlecenia/Create
		public IActionResult Create()
		{
			ViewData["KierowcaID"] = new SelectList(_context.Kierowcy, "ID", "FullName");
			ViewData["KlientID"] = new SelectList(_context.Klienci, "ID", "FullName");
			ViewData["LokalizacjaCelID"] = new SelectList(_context.Lokalizacje, "ID", "Adres");
			ViewData["LokalizacjaStartID"] = new SelectList(_context.Lokalizacje, "ID", "Adres");
			return View();
		}

		// POST: Zlecenia/Create
		[HttpPost]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> Create([Bind("ID,KlientID,KierowcaID,Koszt,LokalizacjaStartID,LokalizacjaCelID,LiczbaOsob")] Zlecenie zlecenie)
		{
			if (zlecenie.LokalizacjaStartID == zlecenie.LokalizacjaCelID)
			{
				HttpContext.Session.SetString("AlertText","Lokalizacje nie mogą być takie same");
				return View(zlecenie);
			}

			if (ModelState.IsValid)
			{
				_context.Add(zlecenie);
				await _context.SaveChangesAsync();
				return RedirectToAction("Index");
			}
			ViewData["KierowcaID"] = new SelectList(_context.Kierowcy, "ID", "Imie", zlecenie.KierowcaID);
			ViewData["KlientID"] = new SelectList(_context.Klienci, "ID", "ID", zlecenie.KlientID);
			ViewData["LokalizacjaCelID"] = new SelectList(_context.Lokalizacje, "ID", "Miasto", zlecenie.LokalizacjaCelID);
			ViewData["LokalizacjaStartID"] = new SelectList(_context.Lokalizacje, "ID", "Miasto", zlecenie.LokalizacjaStartID);
			return View(zlecenie);
		}

		// GET: Zlecenia/CreateByKlient
		public IActionResult CreateByKlient()
		{
			if (HttpContext.Session.GetString("RedirectFromZlecenie") == null)
				HttpContext.Session.SetString("RedirectFromZlecenie", "/Klienci/PanelKlienta");

			if (HttpContext.Session.GetString("KlientID") == null || HttpContext.Session.GetString("KlientFullName") == null)
			{
				return Redirect("/Klienci/Login");
			}

			HttpContext.Session.SetString("RedirectFromLokalizacja", "/Zlecenia/CreateByKlient");

			ViewData["KierowcaID"] = null;
			ViewData["LokalizacjaCelID"] = new SelectList(_context.Lokalizacje.OrderBy(s => s.Ulica), "ID", "Adres");
			ViewData["LokalizacjaStartID"] = new SelectList(_context.Lokalizacje.OrderBy(s => s.Ulica), "ID", "Adres");
			return View();
		}

		// POST: Zlecenia/CreateByKlient
		[HttpPost]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> CreateByKlient([Bind("ID,KlientID,Koszt,LokalizacjaStartID,LokalizacjaCelID,LiczbaOsob")] Zlecenie zlecenie)
		{
			if (zlecenie.LokalizacjaStartID == zlecenie.LokalizacjaCelID)
			{
				HttpContext.Session.SetString("AlertText", "Lokalizacje nie mogą być takie same");
				if (HttpContext.Session.GetString("KlientID") == null || HttpContext.Session.GetString("KlientFullName") == null)
				{
					return Redirect("/Klienci/Login");
				}
				ViewData["LokalizacjaCelID"] = new SelectList(_context.Lokalizacje.OrderBy(s => s.Ulica), "ID", "Adres");
				ViewData["LokalizacjaStartID"] = new SelectList(_context.Lokalizacje.OrderBy(s => s.Ulica), "ID", "Adres");
				return View(zlecenie);
			}
			if (zlecenie.KierowcaID == null)
			{
				zlecenie.KierowcaID = FindNearestAvailableKierowcaForZlecenie(zlecenie);
			}
			if (zlecenie.Koszt == 0)
			{
				zlecenie.Koszt = CalculateCostOfZlecenie(zlecenie);
			}

			if (ModelState.IsValid)
			{
				_context.Add(zlecenie);
				await _context.SaveChangesAsync();
				var kierowca = _context.FindKierowcaById(zlecenie.KierowcaID.Value);
				var samochod = _context.FindSamochodById(kierowca.SamochodID);
				var przewidywanyCzasOczekiwania = _context.CalculateEstimatedArrivalTime(zlecenie);
				HttpContext.Session.SetString("CzasOczekiwania", przewidywanyCzasOczekiwania.ToString());
				HttpContext.Session.SetString("DialogText", "Pomyslnie utworzono zlecenie.\n " +
															"Nasz kierowca " + kierowca.FullName +
															" przyjedzie po Ciebie samochodem " +
															samochod.Nazwa + ".\n" + 
															"Koszt zlecenia: " + String.Format("{0:0.##}", zlecenie.Koszt)
				                                            + " Przewidywany czas oczekiwania: ");
				_context.AddPrzyjecieZleceniaForZlecenie(zlecenie.ID);
				HttpContext.Session.SetString("CurrentZlecenieID", zlecenie.ID.ToString());
				return Redirect(HttpContext.Session.GetString("RedirectFromZlecenie"));
			}

			ViewData["KierowcaID"] = null;
			ViewData["LokalizacjaCelID"] = new SelectList(_context.Lokalizacje, "ID", "Miasto", zlecenie.LokalizacjaCelID);
			ViewData["LokalizacjaStartID"] = new SelectList(_context.Lokalizacje, "ID", "Miasto", zlecenie.LokalizacjaStartID);
			return View(zlecenie);
		}

		private int? FindNearestAvailableKierowcaForZlecenie(Zlecenie zlecenie)
		{
			var startId = zlecenie.LokalizacjaStartID;

			var dostepniKierowcy = _context.GetListOfAvailableKierowcy();

			double minOdleglosc = 999999;
			var najblizszyKierowcaId = 1;

			foreach (var id in dostepniKierowcy)
			{
				var lokalizacjaKierowcyId = _context.FindRecentLokalizacjaOfKierowca(id);
				var odlegloscOdStartu = _context.CalculateDistanceBetween(startId, lokalizacjaKierowcyId);
				if (odlegloscOdStartu < minOdleglosc)
				{
					minOdleglosc = odlegloscOdStartu;
					najblizszyKierowcaId = id;
				}
			}

			return najblizszyKierowcaId;
		}

		private decimal CalculateCostOfZlecenie(Zlecenie zlecenie)
		{
			var start = zlecenie.LokalizacjaStartID;
			var cel = zlecenie.LokalizacjaCelID;
			var odleglosc = _context.CalculateDistanceBetween(start, cel);
			var kierowcaId = zlecenie.KierowcaID.Value;
			var stawkaZa1Km = _context.FindKosztZa1KmOfSamochodOfKierowca(kierowcaId);

			var kosztZlecenia = (int)odleglosc * stawkaZa1Km;

			return kosztZlecenia;
		}

		// GET: Zlecenia/Edit/5
		public async Task<IActionResult> Edit(int? id)
		{
			if (id == null)
			{
				return NotFound();
			}

			var zlecenie = await _context.Zlecenia.SingleOrDefaultAsync(m => m.ID == id);
			if (zlecenie == null)
			{
				return NotFound();
			}
			ViewData["KierowcaID"] = new SelectList(_context.Kierowcy, "ID", "Imie", zlecenie.KierowcaID);
			ViewData["KlientID"] = new SelectList(_context.Klienci, "ID", "ID", zlecenie.KlientID);
			ViewData["LokalizacjaCelID"] = new SelectList(_context.Lokalizacje, "ID", "Miasto", zlecenie.LokalizacjaCelID);
			ViewData["LokalizacjaStartID"] = new SelectList(_context.Lokalizacje, "ID", "Miasto", zlecenie.LokalizacjaStartID);
			return View(zlecenie);
		}

		// POST: Zlecenia/Edit/5
		// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
		// more details see http://go.microsoft.com/fwlink/?LinkId=317598.
		[HttpPost]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> Edit(int id, [Bind("ID,KlientID,KierowcaID,Koszt,LokalizacjaStartID,LokalizacjaCelID,LiczbaOsob")] Zlecenie zlecenie)
		{
			if (id != zlecenie.ID)
			{
				return NotFound();
			}

			if (ModelState.IsValid)
			{
				try
				{
					_context.Update(zlecenie);
					await _context.SaveChangesAsync();
				}
				catch (DbUpdateConcurrencyException)
				{
					if (!ZlecenieExists(zlecenie.ID))
					{
						return NotFound();
					}
					else
					{
						throw;
					}
				}
				return RedirectToAction("Index");
			}
			ViewData["KierowcaID"] = new SelectList(_context.Kierowcy, "ID", "Imie", zlecenie.KierowcaID);
			ViewData["KlientID"] = new SelectList(_context.Klienci, "ID", "ID", zlecenie.KlientID);
			ViewData["LokalizacjaCelID"] = new SelectList(_context.Lokalizacje, "ID", "Miasto", zlecenie.LokalizacjaCelID);
			ViewData["LokalizacjaStartID"] = new SelectList(_context.Lokalizacje, "ID", "Miasto", zlecenie.LokalizacjaStartID);
			return View(zlecenie);
		}

		// GET: Zlecenia/Delete/5
		public async Task<IActionResult> Delete(int? id)
		{
			if (id == null)
			{
				return NotFound();
			}

			var zlecenie = await _context.Zlecenia
				.Include(z => z.Kierowca)
				.Include(z => z.Klient)
				.Include(z => z.LokalizacjaCel)
				.Include(z => z.LokalizacjaStart)
				.SingleOrDefaultAsync(m => m.ID == id);
			if (zlecenie == null)
			{
				return NotFound();
			}

			return View(zlecenie);
		}

		// POST: Zlecenia/Delete/5
		[HttpPost, ActionName("Delete")]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> DeleteConfirmed(int id)
		{
			var zlecenie = await _context.Zlecenia.SingleOrDefaultAsync(m => m.ID == id);
			_context.Zlecenia.Remove(zlecenie);
			await _context.SaveChangesAsync();
			return RedirectToAction("Index");
		}

		private bool ZlecenieExists(int id)
		{
			return _context.Zlecenia.Any(e => e.ID == id);
		}
	}
}
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     